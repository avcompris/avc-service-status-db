# File: avc-service-status-db/Dockerfile
#
# Use to build the image: avcompris/service-status-db

FROM postgres:9.6
MAINTAINER david.andriana@avantage-compris.com

#-------------------------------------------------------------------------------
#   1. POSTGRES
#-------------------------------------------------------------------------------

ENV POSTGRES_USER     postgres
ENV POSTGRES_PASSWORD root123
ENV POSTGRES_DB       statusdb

# PGDATA=/data, otherwise, /var/lib/postgresql/data will be used (and empty)
#
ENV PGDATA=/data

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

EXPOSE 5432
