# avc-service-status-db

Docker image: avcompris/service-status-db

Usage:

	$ docker run -d --name service-status-db \
	    -p 5432:5432 \
	    avcompris/service-status-db


Exposed port is 5432.
